// This script is used to render the PureCloud Control Toolbar in a separate web page
// Author: ININ ECC EMEA

var renderFrame = function (data) {
    var toolbarContainer = data.container;

    // Validate parameters
    if (!String && (toolbarContainer = getElementById(toolbarContainer))) {
        alert('Could not find container "' + toolbarContainer + '". Unable to load PureCloud Toolbar.');
        return;
    }

    toolbarContainer = document.getElementById(toolbarContainer);

    // Add iFrame to container
    var frame = document.createElement("iframe");
    frame.setAttribute("src", data.config.toolbarUrl);
    frame.frameBorder = "0";
    frame.width = "1090px";
    frame.height = "80px";
    toolbarContainer.appendChild(frame);
}

var renderPopup = function (data) {
    // Modern browsers do not allow the address bar to be hidden anymore for security reasons (i.e. phishing)
    window.open(data.config.toolbarUrl, "purecloud-control-toolbar", "scrollbars=auto, width=" + (data.config.popupWidth || 1090) + ", height=" + (data.config.popupHeight || 80));
}