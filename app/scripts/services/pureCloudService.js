'use strict';

/**
 * @ngdoc function
 * @name pureCloudService.service:pureCloudService
 * @description
 * # pureCloudService
 */
angular.module('pureCloudService', [])
  .service('pureCloudService', function($http, $log, oAuth, $httpParamSerializerJQLike, $q, $window, $rootScope) {
    var _sourcePureCloud = 'PURECLOUD';
    var _statusOnQueue = 'ON_QUEUE';
    var _statusOffQueue = 'AVAILABLE';
    var _statusOutOfOffice = 'OUT_OF_OFFICE';
    var _selectableStatuses = ['AVAILABLE', 'BUSY', 'AWAY', 'OUT_OF_OFFICE'];
    var _accessToken;
    var _userData;
    var _environment;
    var _host;
    var _authUrl;
    var _logoutUrl;
    var _lsTokenKeyName = 'ININ.ECCEMEA.PureCloudToolbar.authtoken';
    var _webSocket;
    var _presenceMap = {};
    var _allUserStatuses = [];
    var _allSystemStatuses = [];
    var _currentUserStatus;
    var _outOfOffice;
    var _userQueues;
    var _userQueuesMap = {};

    /**
     * Gets or Sets environment that this is run in.  If set should be mypurecloud.com, mypurecloud.ie, mypurecloud.com.au, etc.
     * @memberof pureCloudService#
     * @param environment
     * { environment : 'purecloud.com' {string} environment PureCloud environment (mypurecloud.com, mypurecloud.ie, mypurecloud.au, etc)
     *   clienId : 0c731aeb-d7e6-4fe5-8f24-1fb3055f997e 
     * }  
     */
    function setEnvironment(environment) {
      if (!environment) {
        throw new Error('Missing required parameter: environment');
      }
      if (!environment.environment) {
        throw new Error('Missing required parameter: environment.environment');
      }
      if (!environment.clientId) {
        throw new Error('Missing required parameter: environment.clientId');
      }
      _environment = environment.environment;
      _host = 'api.' + _environment;
      _authUrl = 'https://login.' + _environment;
      _logoutUrl = 'https://login.' + _environment + '/logout';

      var baseUrl = 'http://localhost:9000/';

      oAuth.extendConfig({
        clientId: environment.clientId,
        redirectUri: baseUrl,
        authorizationEndpoint: _authUrl + '/authorize',
        scopes: [],
        verifyFunc: {}
      });
    }
    this.setEnvironment = setEnvironment;

    this.authenticate = function() {
      var deferred = $q.defer();

      try {

        var existingToken = null;

        if ($window && $window.localStorage) {
          existingToken = $window.localStorage.getItem(_lsTokenKeyName);
        }

        if (existingToken && existingToken !== '') {
          _accessToken = existingToken;

          loadStartupData()
            .then(function success() {
              $log.info('PureCloud Access token: ' + _accessToken);
              deferred.resolve({ 'access_token': _accessToken });
            }, function error() {
              logout();

              authPopup()
                .then(function sucess(params) {
                  deferred.resolve({ 'access_token': params.access_token });
                }, function error() {
                  deferred.reject();
                });

            });
        }
        else {
          authPopup()
            .then(function sucess(params) {
              deferred.resolve({ 'access_token': params.access_token });
            }, function error() {
              deferred.reject();
            });
        }

      }
      catch (e) {
        deferred.reject();
      }

      return deferred.promise;
    };

    function authPopup() {
      var deferred = $q.defer();
      oAuth.getTokenByPopup()
        .then(function(params) {
          // Success getting token from popup.
          _accessToken = params.access_token;
          $log.info('PureCloud Access token: ' + _accessToken);

          if ($window && $window.localStorage) {
            $window.localStorage.setItem(_lsTokenKeyName, _accessToken);
          }

          loadStartupData()
            .then(function success() {
              deferred.resolve({ 'access_token': _accessToken });
            }, function error() {
              logout();
              deferred.reject();
            });
        }, function error() {
          // Failure getting token from popup.
          $log.fatal('Failed to get token from PureCloud');
          logout();
          deferred.reject();
        });
      return deferred.promise;
    }

    function loadStartupData() {
      return $q.all([
        loadUserData(),

        subscribeForNotifications()
      ]);
    }

    function logout() {

      var iframe = document.createElement('iframe');
      if (iframe) {
        var html = '<meta http-equiv="refresh" content="0; url=' + _logoutUrl + '" /><body></body>';
        iframe.src = 'data:text/html;charset=utf-8,' + encodeURI(html);
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        setTimeout(function() {
          document.body.removeChild(iframe);
        }, 1000);
      }

      // Clear the token variable and the local storage entry
      _accessToken = undefined;

      if ($window && $window.localStorage) {
        $window.localStorage.removeItem(_lsTokenKeyName);
      }

      if (_webSocket) {
        _webSocket.onclose = undefined;
        _webSocket.onerror = undefined;
        _webSocket.close();
        _webSocket = undefined;
      }
      _userData = undefined;
      _presenceMap = {};
      _allUserStatuses = [];
      _allSystemStatuses = [];
      _currentUserStatus = undefined;
      _userQueues = undefined;
      _userQueuesMap = {};
      oAuth.clear();
      $log.info('PureCloud Access token cleared');
    }
    this.logout = logout;

    function getUserId() {
      return _userData ? _userData.id : undefined;
    }
    this.getUserId = getUserId;

    this.getUserData = function() {
      return _userData;
    };

    this.getAllUserStatuses = function() {
      return _allUserStatuses;
    };

    this.getUserQueues = function() {
      return _userQueues;
    };

    this.getUserQueuesMap = function() {
      return _userQueuesMap;
    };

    /**
     * @summary return JSON with presenceDefinition only (startup) or presenceDefinition & status (during status change)
     * {
     * 'presenceDefinition': status,
     * 'dateModified': status.dateModified,
     * 'status': [optional]
     * };
     */
    this.getCurrentUserStatus = function() {
      return _currentUserStatus;
    };

    this.getCurrentUserStatusKey = function() {
      if (_currentUserStatus && _currentUserStatus.presenceDefinition) {
        return _currentUserStatus.presenceDefinition.systemPresence;
      }
      return undefined;
    };

    this.getOutOfOffice = function() {
      return _outOfOffice;
    };

    this.getOutOfOfficeStatus = function() {
      return _presenceMap[_statusOutOfOffice];
    };

    this.getOnQueueStatus = function() {
      return _presenceMap[_statusOnQueue];
    };

    this.isOnQueue = function() {
      return this.getCurrentUserStatusKey() === _statusOnQueue;
    };

    function sendRestRequest(requestName, method, path, body) {
      if (!_accessToken) {
        throw new Error('Authentication required!');
      }
      if (!_host) {
        throw new Error('setEnvironment first!');
      }
      if (!requestName) {
        throw new Error('Missing required parameter: requestName');
      }
      if (!method) {
        throw new Error('Missing required parameter: method');
      }
      if (!path) {
        throw new Error('Missing required parameter: path');
      }

      var config = {
        method: method,
        url: 'https://' + _host + path,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + _accessToken
        },
      };

      if (body) {
        config.data = JSON.stringify(body);
      }

      $log.trace('Begin Request: ' + requestName);
      var request = $http(config);
      request.then(function success(response) {
        $log.trace('End Request: ' + requestName + ' (' + JSON.stringify(response.data) + ')');
      }, function error(response) {
        if (response.status === 400 && response.data) {
          $log.error('Request: ' + requestName + ': ' + response.data.code + ': ' + response.data.message + ' (' + JSON.stringify(response.data) + ')');
        }
        else {
          $log.error('Request: ' + requestName + ': HTTP ' + response.status + ' (' + response.statusText + ')');
        }
        $log.trace('End Request: ' + requestName);
      });

      return request;
    }

    function loadUserData() {

      var errorHandler = function() {
        deferred.reject();
      };

      var deferred = $q.defer();

      usersApi.getMe()
        .then(function success(response) {
          _userData = response.data;

          $q.all([
            usersApi.getUserIdQueues(getUserId())
              .then(function success(response) {
                _userQueues = response.data.entities;
                _userQueuesMap = createUserQueuesMap(_userQueues);

              }),

            usersApi.getUserIdOutOfOffice(getUserId())
              .then(function success(response) {
                _outOfOffice = response.data.active;
              })
          ]).then(function success() {
            deferred.resolve();
          }, errorHandler);

        }, errorHandler);

      return deferred.promise;
    }

    function createUserQueuesMap(userQueues) {
      var map = {};
      var length = Object.keys(userQueues).length;
      for (var i = 0; i < length; i++) {
        var queue = userQueues[i];
        map[queue.id] = queue;
      }
      return map;
    }

    function subscribeForNotifications() {

      var errorHandler = function() {
        deferred.reject();
      };

      var deferred = $q.defer();
      presenceApi.getPresenceDefinitions().then(function success(response) {
        _allSystemStatuses = response.data.entities;
        _presenceMap = {};
        _allUserStatuses = [];
        for (var i = 0; i < Object.keys(_allSystemStatuses).length; i++) {
          var presence = _allSystemStatuses[i];
          _presenceMap[presence.systemPresence] = presence;
          if (!presence.deactivated && _selectableStatuses.indexOf(presence.systemPresence) >= 0) {
            _allUserStatuses.push(presence);
          }
        }
        _allUserStatuses.sort(function(a, b) {
          var indexA = _selectableStatuses.indexOf(a.systemPresence);
          var indexB = _selectableStatuses.indexOf(b.systemPresence);
          if (indexA >= 0 && indexB >= 0) {
            return indexA - indexB;
          }
          else if (indexA >= 0) {
            return -1;
          }
          else if (indexB >= 0) {
            return 1;
          }
          return a.systemPresence === b.systemPresence ? 0 : a.systemPresence < b.systemPresence ? -1 : 1;
        });

        $log.info('Creating notification channel.');
        return notificationsApi.createChannel()
          .then(function success(request) {
            var data = request.data;

            var userStatusTopic = 'users.' + getUserId() + '.status';
            var userQueuesEventsTopic = 'users.' + getUserId() + '.queues.events';
            var userConversationsTopic = 'users.' + getUserId() + '.conversations';
            var usersOutOfOfficeTopic = 'users.' + getUserId() + '.outofoffice';

            //start a new web socket using the connect Uri of the channel
            $log.debug('Creating WebSocket for ' + data.connectUri);
            _webSocket = new WebSocket(data.connectUri);
            _webSocket.onopen = function() {
              //now that the connection is open, we can start our subscription.
              $log.info('Subscribing for notifications');
              var subscriptions = [
                { 'id': userStatusTopic },
                { 'id': userQueuesEventsTopic },
                { 'id': userConversationsTopic },
                { 'id': usersOutOfOfficeTopic }
              ];
              $log.info('Add subscriptions for channelId: ' + data.id + ' ' + JSON.stringify(subscriptions));
              return notificationsApi.addChannelSubscriptions(data.id, subscriptions).then(function() {
                presenceApi.getUserPresences(getUserId()).then(function success(response) {
                  var entities = response.data.entities;
                  var findCondition = function(item) { return item.id === entities[i].presenceDefinition.id; };
                  for (var i = 0; i < entities.length; i++) {
                    if (entities[i].source === _sourcePureCloud) {
                      var status = _allSystemStatuses.find(findCondition);
                      _currentUserStatus = {
                        'presenceDefinition': status,
                        'dateModified': new Date(entities[i].modifiedDate),
                      };
                      break;
                    }
                  }

                  deferred.resolve();
                });
              }, errorHandler);
            };

            _webSocket.onmessage = function(message) {
              var data = JSON.parse(message.data);
              if (data.topicName !== 'channel.metadata') {

                switch (data.topicName) {
                  case userStatusTopic:
                    $log.debug('userStatusNotification: ' + message.data);
                    var status = _allSystemStatuses.find(function(item) { return item.id === data.eventBody.status.id; });
                    _currentUserStatus = {
                      'presenceDefinition': status,
                      'dateModified': new Date(data.eventBody.status.dateModified),
                      'status': data.eventBody.status
                    };
                    userStatusChangedEvent.notify(data);
                    break;
                  case userConversationsTopic:
                    $log.debug('userQueueContentChangedNotification: ' + message.data);
                    userQueueContentChangedEvent.notify(data);
                    break;
                  case userQueuesEventsTopic:
                    $log.debug('userQueuesEventsNotification: ' + message.data);
                    var changed = data.eventBody.events.find(function(event) { return event.eventType === 'removed' || event.eventType === 'added'; });
                    if (changed) {
                      $log.debug('User queues were changed. Refreshing data...');
                      usersApi.getUserIdQueues(getUserId())
                        .then(function success(response) {
                          _userQueues = response.data.entities;
                          _userQueuesMap = createUserQueuesMap(_userQueues);
                        });
                    }
                    else {
                      var events = data.eventBody.events;
                      var length = events.length;
                      for (var i = 0; i < length; i++) {
                        if (events[i].eventType === 'joined' || events[i].eventType === 'unjoined') {
                          var queue = _userQueuesMap[events[i].queueId];
                          $log.debug('User ' + events[i].eventType + ' ' + queue.name + ' queue.');
                          queue.joined = events[i].eventType === 'joined' ? true : false;
                        }
                        else {
                          $log.warn('Unhandled userQueuesEventsTopic eventType: ' + events[i].eventType);
                        }
                      }
                    }
                    break;
                  case usersOutOfOfficeTopic:
                    $log.debug('usersOutOfOfficeNotification: ' + message.data);
                    _outOfOffice = data.eventBody.active;
                    userOutOfOfficeChangedEvent.notify(data);
                    break;
                  default:
                    $log.warn('Unhandled subscription topic: ' + message.data);
                    break;
                }
              }
            };

            _webSocket.onclose = function(message) {
              $log.warn('Notification socket has been closed: ' + JSON.stringify(message.data));
            };

            _webSocket.onerror = function(message) {
              $log.error('Notification socket error: ' + JSON.stringify(message.data));
            };
          }, errorHandler);
      }, errorHandler);

      return deferred.promise;
    }

    var userStatusChangedEvent = {
      notify: function(message) {
        $rootScope.$emit('userStatusChanged', message);
      },

      subscribe: function(scope, callback) {
        var handler = $rootScope.$on('userStatusChanged', callback);
        scope.$on('$destroy', handler);
        return handler;
      }
    };
    this.userStatusChanged = userStatusChangedEvent;

    var userQueueContentChangedEvent = {
      notify: function(message) {
        $rootScope.$emit('userQueueContentChanged', message);
      },

      subscribe: function(scope, callback) {
        var handler = $rootScope.$on('userQueueContentChanged', callback);
        scope.$on('$destroy', handler);
        return handler;
      }
    };
    this.userQueueContentChanged = userQueueContentChangedEvent;

    var userOutOfOfficeChangedEvent = {
      notify: function(message) {
        $rootScope.$emit('userOutOfOfficeChanged', message);
      },

      subscribe: function(scope, callback) {
        var handler = $rootScope.$on('userOutOfOfficeChanged', callback);
        scope.$on('$destroy', handler);
        return handler;
      }
    };
    this.userOutOfOfficeChanged = userOutOfOfficeChangedEvent;

    var conversationsApi = {

      /**
       * @summary Get conversations
       * @memberOf conversations#
       * @param {string} communicationType - Call or Chat communication filtering
       */
      getConversations: function(communicationType) {
        var apipath = '/api/v1/conversations';
        var requestBody;
        var queryParameters = {};

        if (communicationType) {
          queryParameters.communicationType = communicationType;
        }

        return sendRestRequest('conversations.getConversations', 'GET', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary Create conversation
       * @memberOf conversations#
       * @param {string} call - Phone number to call
       * @param {string} callFrom - Queue id to place the call from
       * @param {string} callQueueId - Queue id to call
       * @param {string} callUserId - User id to call (this will call the default number)
       * @param {integer} priority - Priority level to use for routing when calling a queue
       * @param {string} languageId - Language id to use for routing when calling a queue
       * @param {array} skillIds - Skill ids to use for routing when calling a queue
       * @param {} body - 
       */
      create: function(call, callFrom, callQueueId, callUserId, priority, languageId, skillIds, body) {
        var apipath = '/api/v1/conversations';
        var requestBody;
        var queryParameters = {};

        if (call) {
          queryParameters.call = call;
        }

        if (callFrom) {
          queryParameters.callFrom = callFrom;
        }

        if (callQueueId) {
          queryParameters.callQueueId = callQueueId;
        }

        if (callUserId) {
          queryParameters.callUserId = callUserId;
        }

        if (priority) {
          queryParameters.priority = priority;
        }

        if (languageId) {
          queryParameters.languageId = languageId;
        }

        if (skillIds) {
          queryParameters.skillIds = skillIds;
        }

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('conversations.create', 'POST', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary Get conversation
       * @memberOf conversations#
       * @param {string} conversationId - conversation ID
       */
      get: function(conversationId) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }

        var apipath = '/api/v1/conversations/{conversationId}';
        var requestBody;

        apipath = apipath.replace('{conversationId}', conversationId);

        return sendRestRequest('conversations.get', 'GET', apipath, requestBody);
      },

      /**
       * @summary Update conversation
       * @memberOf conversations#
       * @param {string} conversationId - conversation ID
       * @param {} body - Conversation
       * @example
       * Body Example:
       * {
       * 'name': '',
       * 'startTime': '',
       * 'endTime': '',
       * 'address': '',
       * 'participants': [],
       * 'conversationIds': [],
       * 'maxParticipants': 0,
       * 'recordingState': ''
       * }
       */
      update: function(conversationId, body) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }
        if (!body) {
          throw new Error('Missing required parameter: body');
        }

        var apipath = '/api/v1/conversations/{conversationId}';
        var requestBody;

        apipath = apipath.replace('{conversationId}', conversationId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('conversations.update', 'PUT', apipath, requestBody);
      },

      /**
       * @summary Update a participant.
       * @description Specify the state as CONNECTED, DISCONNECTED. You can specify a wrap-up code.
       * @memberOf conversations#
       * @param {string} conversationId - conversation ID
       * @param {string} participantId - participant ID
       * @param {} body - Specify the state as CONNECTED, DISCONNECTED. You can specify a wrap-up code.
       * @example
       * Body Example:
       * {
       * 'id': '',
       * 'wrapup': {
       *  'code': '',
       *  'name': '',
       *  'notes': '',
       *  'tags': [],
       *  'durationSeconds': 0,
       *  'endTime': '',
       *  'provisional': true
       *  },
       * 'state': '',
       * 'recording': true,
       * 'muted': true,
       * 'confined': true,
       * 'held': true,
       * 'wrapupSkipped': true
       * }
       */
      updateParticipant: function(conversationId, participantId, body) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }
        if (!participantId) {
          throw new Error('Missing required parameter: participantId');
        }
        var apipath = '/api/v1/conversations/{conversationId}/participants/{participantId}';
        var requestBody;

        apipath = apipath.replace('{conversationId}', conversationId).replace('{participantId}', participantId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('conversations.updateParticipant', 'PUT', apipath, requestBody);
      },

      pickup: function(conversationId, participantId) {
        return this.updateParticipant(conversationId, participantId, {
          'state': 'CONNECTED',
        });
      },

      disconnect: function(conversationId, participantId) {
        return this.updateParticipant(conversationId, participantId, {
          'state': 'DISCONNECTED',
        });
      },

      mute: function(conversationId, participantId, muted) {
        if (!muted) {
          throw new Error('Missing required parameter: muted');
        }
        return this.updateParticipant(conversationId, participantId, {
          'muted': muted,
        });
      },

      hold: function(conversationId, participantId, held) {
        if (!held) {
          throw new Error('Missing required parameter: held');
        }
        return this.updateParticipant(conversationId, participantId, {
          'held': held,
        });
      },

      /**
       * @summary Replace this participant with the specified user and/or address
	     * @memberOf conversations#
	     * @param {string} conversationId - conversation ID
	     * @param {string} participantId - participant ID
	     * @param {string} userId - The user that will replace this participant.  If address is not supplied then the user's Work address will be used.  This parameter is required when replacing a participant that has an active chat.
	     * @param {string} address - The address that will be used to contact the new participant
	     * @param {string} username - The username of the person that will replace this participant.  This field is only used if the userId is blank.
	     * @param {string} queueId - The id of the queue that will replace this participant.
	     * @param {boolean} voicemail - Indicates this participant will be replaced by the voicemail inbox of the participant.
	     */
      transfer: function(conversationId, participantId, userId, address, username, queueId, voicemail, body) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }
        if (!participantId) {
          throw new Error('Missing required parameter: participantId');
        }
        if (!userId && !address && !username && !queueId && !voicemail) {
          throw new Error('At least one destination is required');
        }

        var apipath = '/api/v1/conversations/{conversationId}/participants/{participantId}/replace';
        var requestBody;
        var queryParameters = {};

        apipath = apipath.replace('{conversationId}', conversationId).replace('{participantId}', participantId);

        if (userId) {
          queryParameters.userId = userId;
        }

        if (address) {
          queryParameters.address = address;
        }

        if (username) {
          queryParameters.username = username;
        }

        if (queueId) {
          queryParameters.queueId = queueId;
        }

        if (voicemail) {
          queryParameters.voicemail = voicemail;
        }

        if (body) {
          requestBody = body;
        }

        //NOTE: API v2 will use body params instead of a query request
        return sendRestRequest('conversations.transfer', 'POST', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary Initiate and update consult transfer
       * @memberOf conversations#
       * @param {string} conversationId - conversation ID
       * @param {string} participantId - The object of the transfer
       * @param {} body - Destination address & initial speak to
       * @example
       * Body Example:
       * {
       *   "speakTo": "",
       *   "destination": {
       *       "accountCodeDigits": "",
       *       "postConnectDigits": "",
       *       "address": "",
       *       "name": "",
       *       "userId": "",
       *       "queueId": ""
       *   }
       * }
	     */
      consultTransfer: function(conversationId, participantId, body) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }
        if (!participantId) {
          throw new Error('Missing required parameter: participantId');
        }

        var apipath = '/api/v1/conversations/{conversationId}/participants/{participantId}/consult';
        var requestBody;

        apipath = apipath.replace('{conversationId}', conversationId).replace('{participantId}', participantId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('conversations.consultTransfer', 'POST', apipath, requestBody);
      },

      /**
       * @summary Change who can speak
	     * @memberOf conversations#
	     * @param {string} conversationId - conversation ID
	     * @param {string} participantId - The object of the transfer
	     * @param {} body - new speak to
	     * @example
	     * Body Example:
	     * {
       *   "speakTo": ""
       * }
	     */
      consultTransferChangeWhoCanSpeak: function(conversationId, participantId, body) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }
        if (!participantId) {
          throw new Error('Missing required parameter: participantId');
        }

        var apipath = '/api/v1/conversations/{conversationId}/participants/{participantId}/consult';
        var requestBody;

        apipath = apipath.replace('{conversationId}', conversationId).replace('{participantId}', participantId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('conversations.consultTransferChangeWhoCanSpeak', 'PUT', apipath, requestBody);
      },

      /**
       * @summary Cancel the transfer
	     * @memberOf conversations#
	     * @param {string} conversationId - conversation ID
	     * @param {string} participantId - The object of the transfer
	     */
      cancelConsultTransfer: function(conversationId, participantId) {
        if (!conversationId) {
          throw new Error('Missing required parameter: conversationId');
        }
        if (!participantId) {
          throw new Error('Missing required parameter: participantId');
        }

        var apipath = '/api/v1/conversations/{conversationId}/participants/{participantId}/consult';
        var requestBody;

        apipath = apipath.replace('{conversationId}', conversationId).replace('{participantId}', participantId);

        return sendRestRequest('conversations.cancelConsultTransfer', 'DELETE', apipath, requestBody);
      }

    }; //Conversations
    this.conversations = conversationsApi;

    var presenceApi = {
      /**
       * @summary Get an Organization's list of Presences
       * @memberOf presence#
       * @param {integer} pageNumber - Page number
       * @param {integer} pageSize - Page size
       */
      getPresenceDefinitions: function(pageNumber, pageSize) {
        var apipath = '/api/v1/presencedefinitions';
        var requestBody;
        var queryParameters = {};

        if (pageNumber) {
          queryParameters.pageNumber = pageNumber;
        }

        if (pageSize) {
          queryParameters.pageSize = pageSize;
        }

        return sendRestRequest('presence.getPresenceDefinitions', 'GET', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary Get an User's list of Presences
       * @memberOf presence#
       * @param {string} userId - User ID
       * @param {integer} pageNumber - Page number
       * @param {integer} pageSize - Page size
       */
      getUserPresences: function(userId, pageNumber, pageSize) {
        if (!userId) {
          throw new Error('Missing required parameter: userId');
        }
        var apipath = '/api/v1/users/{userId}/presences';
        var requestBody;
        var queryParameters = {};

        apipath = apipath.replace('{userId}', userId);

        if (pageNumber) {
          queryParameters.pageNumber = pageNumber;
        }

        if (pageSize) {
          queryParameters.pageSize = pageSize;
        }

        return sendRestRequest('presence.getUserPresences', 'GET', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary Update a UserPresence
       * @memberOf presence#
       * @param {string} userId - User ID
       * @param {string} source - Source
       * @param {} body - The updated UserPresence
       * @example
       * Body Example:
       * {
       * 'name': '',
       * 'user: {
       *    'name': '',
       *    'username': '',
       *    'email': '',
       *    'displayName': '',
       *    'phoneNumber': '',
       *    'userImages': [],
       *    'chat': {},
       *    'roles': [],
       *    'voicemailEnabled': true,
       *    'department': '',
       *    'title': '',
       *    'routingStatus': {},
       *    'password': '',
       *    'primaryPresence': {},
       *    'conversations': {},
       *    'conversationSummary': {},
       *    'outOfOffice': {},
       *    'geolocation': {},
       *    'permissions': [],
       *    'requestedStatus': {},
       *    'defaultStationUri': '',
       *    'stationUri': ''
       *    },
       * 'source': '',
       * 'presenceDefinition': {
       *    'name': '',
       *    'languageLabels': {},
       *    'systemPresence': '',
       *    'deactivated': true,
       *    'createdBy': {},
       *    'createdDate': '',
       *    'modifiedBy': {},
       *    'modifiedDate': ''
       *    },
       * 'message': '',
       * 'modifiedBy': {
       *    'name': '',
       *    'username': '',
       *    'email': '',
       *    'displayName': '',
       *    'phoneNumber': '',
       *    'userImages': [],
       *    'chat': {},
       *    'roles': [],
       *    'voicemailEnabled': true,
       *    'department': '',
       *    'title': '',
       *    'routingStatus': {},
       *    'password': '',
       *    'primaryPresence': {},
       *    'conversations': {},
       *    'conversationSummary': {},
       *    'outOfOffice': {},
       *    'geolocation': {},
       *    'permissions': [],
       *    'requestedStatus': {},
       *    'defaultStationUri': '',
       *    'stationUri': ''
       *    },
       * 'modifiedDate': ''
       * }
       */
      updateUserPresences: function(userId, source, body) {
        if (!userId) {
          throw new Error('Missing required parameter: userId');
        }
        if (!source) {
          throw new Error('Missing required parameter: source');
        }
        if (!body) {
          throw new Error('Missing required parameter: body');
        }
        var apipath = '/api/v1/users/{userId}/presences/{source}';
        var requestBody;
        var queryParameters = {};

        apipath = apipath.replace('{userId}', userId).replace('{source}', source);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('presence.updateUserPresences', 'PUT', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary statusKey will be 'AVAILABLE' or 'BUSY', not the localized text string
       */
      setMyStatus: function(statusKey) {
        if (!statusKey) {
          throw new Error('Missing required parameter: statusKey');
        }
        if (!_presenceMap[statusKey]) {
          throw new Error('Invalid statusKey');
        }
        var setPresence = {
          'presenceDefinition': {
            'id': _presenceMap[statusKey].id
          }
        };

        return this.updateUserPresences(getUserId(), _sourcePureCloud, setPresence);
      },

      setMeOnQueue: function() {
        return this.setMyStatus(_statusOnQueue);
      },

      setMeOffQueue: function() {
        return this.setMyStatus(_statusOffQueue);
      },
    }; //Presence
    this.presence = presenceApi;

    var notificationsApi = {
      /**
       * @summary Get available notification topics.
       * @memberOf notifications#
       */
      getAvailableTopics: function() {
        var apipath = '/api/v1/notifications/availabletopics';
        var requestBody;

        return sendRestRequest('notifications.getAvailableTopics', 'GET', apipath, requestBody);
      },

      /**
       * @summary The list of existing channels
       * @memberOf notifications#
       */
      getChannels: function() {
        var apipath = '/api/v1/notifications/channels';
        var requestBody;

        return sendRestRequest('notifications.getChannels', 'GET', apipath, requestBody);
      },

      /**
       * @summary Create a new channel
       * @description There is a limit of 10 channels. Creating an 11th channel will remove the channel with oldest last used date.
       * @memberOf notifications#
       */
      createChannel: function() {
        var apipath = '/api/v1/notifications/channels';
        var requestBody;

        return sendRestRequest('notifications.createChannel', 'POST', apipath, requestBody);
      },

      /**
       * @summary The list of all subscriptions for this channel
       * @memberOf notifications#
       * @param {string} channelId - Channel ID
       */
      getChannelSubscriptions: function(channelId) {
        if (!channelId) {
          throw new Error('Missing required parameter: channelId');
        }

        var apipath = '/api/v1/notifications/channels/{channelId}/subscriptions';
        var requestBody;

        apipath = apipath.replace('{channelId}', channelId);

        return sendRestRequest('notifications.getChannelSubscriptions', 'GET', apipath, requestBody);
      },

      /**
       * @summary Add a list of subscriptions to the existing list of subscriptions
       * @memberOf notifications#
       * @param {string} channelId - Channel ID
       * @param {} body - Topic
       */
      addChannelSubscriptions: function(channelId, body) {
        if (!channelId) {
          throw new Error('Missing required parameter: channelId');
        }

        var apipath = '/api/v1/notifications/channels/{channelId}/subscriptions';
        var requestBody;

        apipath = apipath.replace('{channelId}', channelId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('notifications.addChannelSubscriptions', 'POST', apipath, requestBody);
      },

      /**
       * @summary Replace the current list of subscriptions with a new list.
       * @memberOf notifications#
       * @param {string} channelId - Channel ID
       * @param {} body - Topic
       */
      replaceChannelSubscriptions: function(channelId, body) {
        if (!channelId) {
          throw new Error('Missing required parameter: channelId');
        }

        var apipath = '/api/v1/notifications/channels/{channelId}/subscriptions';
        var requestBody;

        apipath = apipath.replace('{channelId}', channelId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('notifications.replaceChannelSubscriptions', 'PUT', apipath, requestBody);
      },

      /**
       * @summary Remove all subscriptions
       * @memberOf notifications#
       * @param {string} channelId - Channel ID
       */
      deleteChannelSubscriptions: function(channelId) {
        if (!channelId) {
          throw new Error('Missing required parameter: channelId');
        }

        var apipath = '/api/v1/notifications/channels/{channelId}/subscriptions';
        var requestBody;

        apipath = apipath.replace('{channelId}', channelId);

        return sendRestRequest('notifications.deleteChannelSubscriptions', 'DELETE', apipath, requestBody);
      }
    }; //Notifications
    this.notifications = notificationsApi;

    var routingApi = {
      /**
       * @summary Join or unjoin a queue for a user
	     * @memberOf routing#
	     * @param {string} memberId - User ID
	     * @param {string} queueId - User Queue
	     * @param {string} join - true or false to join or unjoin
	     */
      changeUserWorkgroupsActivation: function(memberId, queueId, join) {
        if (!memberId) {
          throw new Error('Missing required parameter: memberId');
        }
        if (!queueId) {
          throw new Error('Missing required parameter: queueId');
        }
        var apipath = '/api/v1/routing/queues/{queueId}/members/{memberId}';
        var requestBody = { 'joined': join };

        apipath = apipath.replace('{memberId}', memberId).replace('{queueId}', queueId);

        return sendRestRequest('routing.changeUserWorkgroupsActivation', 'PUT', apipath, requestBody);
      }
    };
    this.routing = routingApi;

    var usersApi = {
      /**
       * @summary Get user.
       * @memberOf users#
       * @param {array} expand - Which fields, if any, to expand
       */
      getMe: function(expand) {
        var apipath = '/api/v1/users/me';
        var requestBody;
        var queryParameters = {};

        if (expand) {
          queryParameters.expand = expand;
        }

        return sendRestRequest('users.getMe', 'GET', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      },

      /**
       * @summary Get a OutOfOffice
       * @memberOf users#
       * @param {string} userId - User ID
       */
      getUserIdOutOfOffice: function(userId) {
        if (!userId) {
          throw new Error('Missing required parameter: userId');
        }
        var apipath = '/api/v1/users/{userId}/outofoffice';
        var requestBody;

        apipath = apipath.replace('{userId}', userId);

        return sendRestRequest('users.getUserIdOutOfOffice', 'GET', apipath, requestBody);
      },

      /**
       * @summary Update an OutOfOffice
       * @memberOf users#
       * @param {string} userId - User ID
       * @param {} body - The updated UserPresence
       * @example
       * Body Example:
       * {
          "name": "",
          "user": {
            "name": "",
            "username": "",
            "email": "",
            "displayName": "",
            "phoneNumber": "",
            "userImages": [],
            "chat": {},
            "roles": [],
            "voicemailEnabled": true,
            "department": "",
            "title": "",
            "routingStatus": {},
            "password": "",
            "primaryPresence": {},
            "conversations": {},
            "conversationSummary": {},
            "outOfOffice": {},
            "geolocation": {},
            "permissions": [],
            "requestedStatus": {},
            "defaultStationUri": "",
            "stationUri": ""
        },
        "startDate": "",
        "endDate": "",
        "active": true
      }
      */
      setUserIdOutOfOffice: function(userId, body) {
        if (!userId) {
          throw new Error('Missing required parameter: userId');
        }
        if (!body) {
          throw new Error('Missing required parameter: body');
        }
        var apipath = '/api/v1/users/{userId}/outofoffice';
        var requestBody;

        apipath = apipath.replace('{userId}', userId);

        if (body) {
          requestBody = body;
        }

        return sendRestRequest('users.setUserIdOutOfOffice', 'PUT', apipath, requestBody);
      },

      /**
       * @sumary Set OutOfOffice mode on/off
       * @memberOf users#
       * @param {boolean} active
      */
      setMeOutOffOffice: function(active) {
        if (active === undefined) {
          throw new Error('Missing required parameter: active');
        }
        return this.setUserIdOutOfOffice(getUserId(), {
          'active': active,
          'startDate': '0001-01-01T00:00:00Z',
          'endDate': active ? '9999-12-31T00:00:00Z' : '0001-01-01T00:00:00Z'
        });
      },

      /**
       * @summary Get queues for user
	     * @memberOf users#
	     * @param {string} userId - User ID
	     * @param {integer} pageSize - Page size
	     * @param {integer} pageNumber - Page number
	     */
      getUserIdQueues: function(userId, pageSize, pageNumber) {
        if (!userId) {
          throw new Error('Missing required parameter: userId');
        }
        var apipath = '/api/v1/users/{userId}/queues';
        var requestBody;
        var queryParameters = {};

        apipath = apipath.replace('{userId}', userId);

        if (pageSize) {
          queryParameters.pageSize = pageSize;
        }

        if (pageNumber) {
          queryParameters.pageNumber = pageNumber;
        }

        return sendRestRequest('users.getUserIdQueues', 'GET', apipath + '?' + $httpParamSerializerJQLike(queryParameters), requestBody);
      }
    }; //Users
    this.users = usersApi;
  });