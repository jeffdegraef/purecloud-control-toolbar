'use strict';

/**
 * @ngdoc overview
 * @name pureCloudToolbarApp
 * @description
 * # pureCloudToolbarApp
 *
 * Main module of the application.
 */
angular
  .module('pureCloudToolbarApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'oAuthProvider',
    'pureCloudLoggerProvider',
    'pureCloudService',
    'frapontillo.bootstrap-switch',
    'ui.bootstrap',
    'angular-web-notification'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/access_token=:accessToken', {
        templateUrl: 'views/callback.html',
        controller: 'CallbackCtrl',
        controllerAs: 'callback'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .factory('$log', function(logger) {
    return logger;
  })
  .run(function($rootScope, $log, pureCloudService) {
    $rootScope.$log = $log;
    $log.setLevel(log4javascript.Level.ALL);
    $log.setLocalStorageEnabled(false);

    if (!String.startsWith) {
      String.prototype.startsWith = function(prefix) {
        return this.indexOf(prefix) === 0;
      };
    }

    pureCloudService.setEnvironment({ environment: 'ininsca.com', clientId: 'ed530da3-6edd-4fc5-af05-c93b1f67bb1c' });
    //pureCloudService.setEnvironment({ environment: 'mypurecloud.ie', clientId: '0c731aeb-d7e6-4fe5-8f24-1fb3055f997e' });
  });