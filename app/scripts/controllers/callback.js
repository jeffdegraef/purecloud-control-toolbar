'use strict';

/**
 * @ngdoc function
 * @name pureCloudToolbarApp.controller:CallbackCtrl
 * @description
 * # CallbackCtrl
 * Controller of the pureCloudToolbarApp
 */
angular.module('pureCloudToolbarApp')
  .controller('CallbackCtrl', function($location) {
    /**
     * Parses an escaped url query string into key-value pairs.
     *
     * (Copied from Angular.js in the AngularJS project.)
     *
     * @returns Object.<(string|boolean)>
     */
    function parseKeyValue(/**string*/keyValue) {
      var obj = {}, keyValueArray, key;
      angular.forEach((keyValue || '').split('&'), function(keyValue) {
        if (keyValue) {
          keyValueArray = keyValue.split('=');
          key = decodeURIComponent(keyValueArray[0]);
          obj[key] = angular.isDefined(keyValueArray[1]) ? decodeURIComponent(keyValueArray[1]) : true;
        }
      });
      return obj;
    }

    var queryString = $location.path().substring(1);  // preceding slash omitted
    var params = parseKeyValue(queryString);
    // TODO: The target origin should be set to an explicit origin.  Otherwise, a malicious site that can receive
    //       the token if it manages to change the location of the parent. (See:
    //       https://developer.mozilla.org/en/docs/DOM/window.postMessage#Security_concerns)

    if (queryString) {
      window.opener.postMessage(params, '*');
      window.close();
    }

  });
