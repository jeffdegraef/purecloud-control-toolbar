'use strict';

/**
 * @ngdoc function
 * @name pureCloudToolbarApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pureCloudToolbarApp
**/
angular.module('pureCloudToolbarApp').
  controller('MainCtrl', function($rootScope, $scope, $log, pureCloudService, $interval, webNotification) {
    var statusColorMap = { 'AVAILABLE': 'lawngreen', 'BUSY': 'red', 'AWAY': '#FB3', 'OUT_OF_OFFICE': '#ff1dce', 'ON_QUEUE': '#52cef8' };
    var callStateColorMap = { 'UNKNOWN': 'white', 'DIALING': 'yellow', 'ALERTING': 'yellow', 'CONNECTED': 'lawngreen' };

    $scope.authenticate = function() {
      try {
        $scope.authenticateInProgress = true;
        pureCloudService.authenticate()
          .then(function(params) {
            try {
              // Success getting token from popup.
              $scope.access_token = params.access_token;
              $scope.user_name = pureCloudService.getUserData().name;
              $scope.user_image = pureCloudService.getUserData().userImages[1].imageUri;
              populateUserStatues();
              $scope.conversationList = [];
              $scope.numberToBeDialed = '';
              $scope.isQueueActivationPanelVisible = false;

              pureCloudService.userStatusChanged.subscribe($scope, onUserStatusChagned);
              pureCloudService.userQueueContentChanged.subscribe($scope, onUserQueueContentChagned);
              pureCloudService.userOutOfOfficeChanged.subscribe($scope, onUserOutOfOfficeChanged);

              updateCurrentUserStatus();
              $interval(tick, 1000);
            }
            catch (e) {
              $scope.authenticateInProgress = undefined;
              showErrorNotification('Authentication failure: ' + e.message);
              throw e;
            }
          }, function() {
            // Failure getting token from popup.
            showErrorNotification('Failed to get token from PureCloud.');
            $scope.authenticateInProgress = undefined;
          });
      }
      catch (e) {
        $scope.authenticateInProgress = undefined;
        showErrorNotification('Authentication failure: ' + e.message);
        throw e;
      }
    };

    $scope.logout = function() {
      try {
        pureCloudService.logout();
        $scope.access_token = undefined;
        $scope.authenticateInProgress = undefined;
      }
      catch (e) {
        showErrorNotification('Logout failure: ' + e.message);
        throw e;
      }
    };

    $scope.showQueueActivationPanel = function() {
      if ($scope.isQueueActivationPanelVisible === false) {

        var queues = pureCloudService.getUserQueues();
        $scope.queues = queues;

        $scope.isQueueActivationPanelVisible = true;
      }
      else {
        $scope.isQueueActivationPanelVisible = false;
      }
    };


    $scope.changeUserActivationOnQueue = function(queueId) {
      try {
        var queue = pureCloudService.getUserQueuesMap()[queueId];
        if (queue) {
          pureCloudService.routing.changeUserWorkgroupsActivation(pureCloudService.getUserId(), queueId, !queue.joined)
            .then(function success() {
              showInfoNotification('User ' + (queue.joined ? 'unjoined' : 'jonined ') + queue.name + ' queue.');
            }, function error(param) {
              if (param && param.data && param.data.message) {
                showErrorNotification('Failed to change queue activation: ' + param.data.message);
              }
              else {
                showErrorNotification('Failed to change queue activation');
              }
            });
        }
      }
      catch (e) {
        showErrorNotification('Failed to change queue activation: ' + e.message);
        throw e;
      }
    };


    function onUserStatusChagned(/*event, message*/) {
      updateCurrentUserStatus();
      $scope.$apply();
    }

    function onUserQueueContentChagned(event, message) {
      var eventBody = message.eventBody;
      var userParticipant = getParticipant(eventBody, ['user', 'agent']);
      var externalParticipant = getParticipant(eventBody, ['external', 'customer']);

      $log.debug('local part:' + JSON.stringify(userParticipant));
      $log.debug('remote part:' + JSON.stringify(externalParticipant));

      if (!userParticipant || !externalParticipant) { return; }

      var callState = userParticipant.calls[0].state;
      eventBody.callState = callState;
      eventBody.callStateColor = getCallStateColor(callState);
      eventBody.localCallId = eventBody.id;
      eventBody.localParticipantId = userParticipant.id;
      eventBody.remoteAddress = cleanSipAddress(externalParticipant.address);
      eventBody.remoteName = externalParticipant.name;
      eventBody.queueName = getQueueName(userParticipant.queueId);
      eventBody.icon = getConverstionIcon(userParticipant.calls[0].direction);
      eventBody.customAttribute = '-'; // todo: retreving custom attr from API

      var conversationIndex = getConversationIndexById(eventBody.id);

      if (conversationIndex === undefined) {
        if (callState !== 'DISCONNECTED') {
          // new active call - add item
          $scope.conversationList.push(eventBody);
        }
      }
      else {
        if (callState !== 'DISCONNECTED') {
          // existing active call - update item
          $scope.conversationList[conversationIndex] = eventBody;
        }
        else {
          // existing disconnected call - remove item
          $scope.conversationList.splice(conversationIndex, 1);
        }
      }
      $scope.$apply();
    }

    function onUserOutOfOfficeChanged(/*event, message*/) {
      updateCurrentUserStatus();
      $scope.$apply();
    }

    $scope.$watch('isOnQueue', function(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }

      try {
        if ($scope.isOnQueue === pureCloudService.isOnQueue()) {
          return;
        }

        var setCurrentQueueStatusFun = function() {
          $scope.isOnQueue = pureCloudService.isOnQueue();
        };

        $log.debug('OnOffQueue switch was changed.');
        if ($scope.isOnQueue === true) {
          pureCloudService.presence.setMeOnQueue().then(function success() {
            if (pureCloudService.getOutOfOffice() === true) {
              pureCloudService.users.setMeOutOffOffice(false).catch(setCurrentQueueStatusFun);
            }
          }, setCurrentQueueStatusFun);
        }
        else {
          pureCloudService.presence.setMeOffQueue().then(function success() {
            if (pureCloudService.getOutOfOffice() === true) {
              pureCloudService.users.setMeOutOffOffice(false).catch(setCurrentQueueStatusFun);
            }
          }, setCurrentQueueStatusFun);
        }
      }
      catch (ex) {
        var isOnQueue = pureCloudService.isOnQueue();
        if ($scope.isOnQueue !== isOnQueue) {
          setTimeout(function() {
            $scope.isOnQueue = isOnQueue;
            $scope.$apply();
          }, 0);
        }
        throw ex;
      }
    });

    $scope.onStatusDropDownItemClick = function(status) {
      try {
        $log.debug('Status item was clicked: ' + JSON.stringify(status));
        if (status.systemPresence === pureCloudService.getOutOfOfficeStatus().systemPresence) {
          $scope.isOnQueue = false;
          pureCloudService.users.setMeOutOffOffice(true).catch(function() {
            updateCurrentUserStatus();
          });
        }
        else {
          $scope.selectedUserStatus = status;
          pureCloudService.presence.setMyStatus(status.systemPresence).catch(function() {
            updateCurrentUserStatus();
          });

          if (pureCloudService.getOutOfOffice() === true) {
            pureCloudService.users.setMeOutOffOffice(false).catch(function() {
              updateCurrentUserStatus();
            });
          }
        }
      }
      catch (ex) {
        updateCurrentUserStatus();
        throw ex;
      }
    };

    $scope.getStatusColor = function(status) {
      if (!status || !status.systemPresence) {
        return statusColorMap.AVAILABLE;
      }
      var color = statusColorMap[status.systemPresence];
      if (!color) {
        color = statusColorMap.AVAILABLE;
      }
      return color;
    };

    function populateUserStatues() {
      var allUserStatuses = pureCloudService.getAllUserStatuses();
      $scope.userStatuses = allUserStatuses;
    }

    function updateCurrentUserStatus() {
      if (pureCloudService.getOutOfOffice() === true) {
        if (pureCloudService.isOnQueue() === true) {
          $scope.selectedUserStatus = pureCloudService.getOnQueueStatus();
        }
        else {
          $scope.selectedUserStatus = pureCloudService.getOutOfOfficeStatus();
        }
      }
      else {
        $scope.selectedUserStatus = pureCloudService.getCurrentUserStatus().presenceDefinition;
      }
      $scope.isOnQueue = pureCloudService.isOnQueue();
    }

    function tick() {
      var currentStatus = pureCloudService.getCurrentUserStatus();
      if (!currentStatus || !currentStatus.dateModified) {
        $scope.timer = '00:00';
        return;
      }
      var dt = Math.floor((new Date().getTime() - currentStatus.dateModified.getTime()) / 1000);
      if (dt < 0) {
        dt = 0;
      }
      $scope.timer = toHHMMSS(dt);
    }

    function toHHMMSS(secNum) {
      var hours = Math.floor(secNum / 3600);
      var minutes = Math.floor((secNum - (hours * 3600)) / 60);
      var seconds = secNum - (hours * 3600) - (minutes * 60);

      if (hours < 10) { hours = '0' + hours; }
      if (minutes < 10) { minutes = '0' + minutes; }
      if (seconds < 10) { seconds = '0' + seconds; }
      var time = hours + ':' + minutes + ':' + seconds;
      return time;
    }

    function showInfoNotification(message) {
      webNotification.showNotification('Info', {
        body: message,
        icon: 'images/inin-logo-2.png',
        autoClose: 30000 //30 seconds
      }, function onShow(error/*, hide*/) {
        if (error) {
          $log.warn('Unable to show notification: ' + error.message);
        }
      });
    }

    function showErrorNotification(message) {
      webNotification.showNotification('Error', {
        body: message,
        icon: 'images/inin-logo-2.png',
        autoClose: 30000 //30 seconds
      }, function onShow(error/*, hide*/) {
        if (error) {
          $log.warn('Unable to show notification: ' + error.message);
        }
      });
    }

    $scope.NewCall_ShowDialog = function() {
      var index = getConversationIndexById('0');
      if (index >= 0) {
        return;
      }
      $log.debug('Showing a new call dialog.');
      var newCallDialog = {
        id: '0',
        callState: 'none'
      };
      $scope.conversationList.push(newCallDialog);
    };

    $scope.NewCall_Dial = function(numberToBeDialed) {
      $log.debug('Placing a call: ' + numberToBeDialed);
      pureCloudService.conversations.create(numberToBeDialed)
        .then(function success(response) {
          var conversationId = response.data.id;
          var index = getConversationIndexById('0');
          if (index >= 0) {
            $scope.conversationList[index].id = conversationId;
          }
        });
    };

    $scope.NewCall_CancelDialog = function() {
      $log.debug('Canceling the new call dialog.');
      var index = getConversationIndexById('0');
      if (index >= 0) {
        $scope.conversationList.splice(index, 1);
      }
    };

    function getConversationIndexById(conversationId) {
      for (var i = 0, len = $scope.conversationList.length; i < len; i++) {
        if ($scope.conversationList[i].id === conversationId) {
          return i;
        }
      }
      return undefined;
    }

    function getCallStateColor(callState) {
      var color = callStateColorMap[callState];
      if (color) { return color; }
      return statusColorMap.UNKNOWN;
    }

    function getParticipant(eventBody, participantPurposes) {
      var participantList = eventBody.participants;
      for (var i = 0, iLen = participantList.length; i < iLen; i++) {
        for (var j = 0, jLen = participantPurposes.length; j < jLen; j++) {
          if (participantList[i].purpose === participantPurposes[j]) { return participantList[i]; }
        }
      }
      return undefined;
    }

    function getQueueName(queueId) {
      try {
        var queueName = pureCloudService.getUserQueuesMap()[queueId].name;
        if (queueName) { return queueName; }
        return 'non-ACD';
      } catch (e) {
        return 'non-ACD';
      }
    }

    function getConverstionIcon(direction) {
      if (direction === 'INBOUND') { return 'images/conv-type-call-in.png'; }
      return 'images/conv-type-call-out.png';
    }

    function cleanSipAddress(address) {
      if (!address.startsWith('sip:')) { return address; }
      var userPortion = address.split('@')[0].split(':')[1];
      return userPortion;
    }

    $scope.pickup = function(conversationId, participantId) {
      try {
        pureCloudService.conversations.pickup(conversationId, participantId)
          .catch(function error(param) {
            if (param && param.data && param.data.message) {
              showErrorNotification('Failed to pickup conversation: ' + param.data.message);
            }
            else {
              showErrorNotification('Failed to pickup conversation');
            }
          });
      }
      catch (e) {
        showErrorNotification('Failed to pickup conversation: ' + e.message);
        throw e;
      }
    };

    $scope.disconnect = function(conversationId, participantId) {
      try {
        pureCloudService.conversations.disconnect(conversationId, participantId)
          .catch(function error(param) {
            if (param && param.data && param.data.message) {
              showErrorNotification('Failed to disconnect conversation: ' + param.data.message);
            }
            else {
              showErrorNotification('Failed to disconnect conversation');
            }
          });
      }
      catch (e) {
        showErrorNotification('Failed to disconnect conversation: ' + e.message);
        throw e;
      }
    };
  });
