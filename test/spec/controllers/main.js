'use strict';

describe('Controller: MainCtrl', function() {

  // load the controller's module
  beforeEach(module('pureCloudToolbarApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should contain an accessToken variable', function() {
    expect(MainCtrl.access_token).toBe(undefined);
  });

  it('should set authenticate function', function() {
    expect(scope.authenticate).toBeDefined();
  });

});
