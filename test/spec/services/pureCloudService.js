'use strict';

describe('Service: pureCloudService', function() {

  beforeEach(module('pureCloudToolbarApp'));

  var pureCloudService;
  var $scope;
  beforeEach(inject(['pureCloudService', '$rootScope', function(_pureCloudService_, _rootScope_) {
    pureCloudService = _pureCloudService_;
    $scope = _rootScope_.$new();
  }]));

  it('pureCloudService.authenticate', function() {
    expect(pureCloudService.authenticate).toBeDefined();
  });

  it('pureCloudService.setEnvironment', function() {
    expect(pureCloudService.setEnvironment).toBeDefined();
    expect(function() {
      pureCloudService.setEnvironment();
    }).toThrowError('Missing required parameter: environment');
    expect(function() {
      pureCloudService.setEnvironment('mypurecloud.ie');
    }).toThrowError('Missing required parameter: environment.environment');
    expect(function() {
      pureCloudService.setEnvironment({ environment: 'mypurecloud.ie' });
    }).toThrowError('Missing required parameter: environment.clientId');
    expect(function() {
      pureCloudService.setEnvironment({ environment: 'mypurecloud.ie', clientId: '0c731aeb-d7e6-4fe5-8f24-1fb3055f997f' });
    }).not.toThrow();
  });

  it('pureCloudService.logout', function() {
    expect(pureCloudService.logout).toBeDefined();
    expect(function() {
      pureCloudService.logout();
    }).not.toThrow();
  });

  it('pureCloudService.getUserId', function() {
    expect(pureCloudService.getUserId).toBeDefined();
    expect(pureCloudService.getUserId()).toBe(undefined);
  });

  it('pureCloudService.getUserData', function() {
    expect(pureCloudService.getUserData).toBeDefined();
    expect(pureCloudService.getUserData()).toBe(undefined);
  });
  
  it('pureCloudService.getUserQueues', function() {
    expect(pureCloudService.getUserQueues).toBeDefined();
    expect(pureCloudService.getUserQueues()).toBe(undefined);
  });
  
  it('pureCloudService.getUserQueuesMap', function() {
    expect(pureCloudService.getUserQueuesMap).toBeDefined();
    expect(pureCloudService.getUserQueuesMap()).not.toBe(undefined);
  });

  it('pureCloudService.getCurrentUserStatus', function() {
    expect(pureCloudService.getCurrentUserStatus).toBeDefined();
    expect(pureCloudService.getCurrentUserStatus()).toBe(undefined);
  });

  it('pureCloudService.getCurrentUserStatusKey', function() {
    expect(pureCloudService.getCurrentUserStatusKey).toBeDefined();
    expect(pureCloudService.getCurrentUserStatusKey()).toBe(undefined);
  });

  it('pureCloudService.userStatusChanged', function() {
    expect(pureCloudService.userStatusChanged).toBeDefined();
    expect(pureCloudService.userStatusChanged.subscribe).toBeDefined();
    expect(function() {
      pureCloudService.userStatusChanged.subscribe($scope, function() {
      });
    }).not.toThrow();
  });

  it('pureCloudService.userQueueContentChanged', function() {
    expect(pureCloudService.userQueueContentChanged).toBeDefined();
    expect(pureCloudService.userQueueContentChanged.subscribe).toBeDefined();
    expect(function() {
      pureCloudService.userQueueContentChanged.subscribe($scope, function() {
      });
    }).not.toThrow();
  });

  /* BEGIN: conversations */
  it('pureCloudService.conversations.getConversations', function() {
    expect(pureCloudService.conversations.getConversations).toBeDefined();
    expect(function() {
      pureCloudService.conversations.getConversations();
    }).toThrowError('Authentication required!');
    expect(function() {
      pureCloudService.conversations.getConversations('Call');
    }).toThrowError('Authentication required!');
    expect(function() {
      pureCloudService.conversations.getConversations('Chat');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.create', function() {
    expect(pureCloudService.conversations.create).toBeDefined();
    expect(function() {
      pureCloudService.conversations.create();
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.get', function() {
    expect(pureCloudService.conversations.get).toBeDefined();
    expect(function() {
      pureCloudService.conversations.get();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.get('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.update', function() {
    expect(pureCloudService.conversations.update).toBeDefined();
    expect(function() {
      pureCloudService.conversations.update();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.update('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: body');
    expect(function() {
      pureCloudService.conversations.update('101b656e-b5b9-40b5-90ec-519b0ae390c0', '{}');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.updateParticipant', function() {
    expect(pureCloudService.conversations.updateParticipant).toBeDefined();
    expect(function() {
      pureCloudService.conversations.updateParticipant();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.updateParticipant('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.updateParticipant('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.pickup', function() {
    expect(pureCloudService.conversations.pickup).toBeDefined();
    expect(function() {
      pureCloudService.conversations.pickup();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.pickup('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.pickup('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.disconnect', function() {
    expect(pureCloudService.conversations.disconnect).toBeDefined();
    expect(function() {
      pureCloudService.conversations.disconnect();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.disconnect('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.disconnect('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.mute', function() {
    expect(pureCloudService.conversations.mute).toBeDefined();
    expect(function() {
      pureCloudService.conversations.mute();
    }).toThrowError('Missing required parameter: muted');
    expect(function() {
      pureCloudService.conversations.mute('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: muted');
    expect(function() {
      pureCloudService.conversations.mute('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: muted');
    expect(function() {
      pureCloudService.conversations.mute('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0', true);
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.hold', function() {
    expect(pureCloudService.conversations.hold).toBeDefined();
    expect(function() {
      pureCloudService.conversations.hold();
    }).toThrowError('Missing required parameter: held');
    expect(function() {
      pureCloudService.conversations.hold('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: held');
    expect(function() {
      pureCloudService.conversations.hold('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: held');
    expect(function() {
      pureCloudService.conversations.hold('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0', true);
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.transfer', function() {
    expect(pureCloudService.conversations.transfer).toBeDefined();
    expect(function() {
      pureCloudService.conversations.transfer();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.transfer('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.transfer('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('At least one destination is required');
    expect(function() {
      pureCloudService.conversations.transfer('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0', undefined, undefined, undefined, undefined, undefined, undefined);
    }).toThrowError('At least one destination is required');
    expect(function() {
      pureCloudService.conversations.transfer('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0', 'test', undefined, undefined, undefined, undefined, undefined);
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.consultTransfer', function() {
    expect(pureCloudService.conversations.consultTransfer).toBeDefined();
    expect(function() {
      pureCloudService.conversations.consultTransfer();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.consultTransfer('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.consultTransfer('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.consultTransferChangeWhoCanSpeak', function() {
    expect(pureCloudService.conversations.consultTransferChangeWhoCanSpeak).toBeDefined();
    expect(function() {
      pureCloudService.conversations.consultTransferChangeWhoCanSpeak();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.consultTransferChangeWhoCanSpeak('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.consultTransferChangeWhoCanSpeak('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.conversations.cancelConsultTransfer', function() {
    expect(pureCloudService.conversations.cancelConsultTransfer).toBeDefined();
    expect(function() {
      pureCloudService.conversations.cancelConsultTransfer();
    }).toThrowError('Missing required parameter: conversationId');
    expect(function() {
      pureCloudService.conversations.cancelConsultTransfer('101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Missing required parameter: participantId');
    expect(function() {
      pureCloudService.conversations.cancelConsultTransfer('101b656e-b5b9-40b5-90ec-519b0ae390c0', '101b656e-b5b9-40b5-90ec-519b0ae390c0');
    }).toThrowError('Authentication required!');
  });
  /* END: conversations */

  /* BEGIN: presence */
  it('pureCloudService.presence.getPresenceDefinitions', function() {
    expect(pureCloudService.presence.getPresenceDefinitions).toBeDefined();
    expect(function() {
      pureCloudService.presence.getPresenceDefinitions();
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.presence.getUserPresences', function() {
    expect(pureCloudService.presence.getUserPresences).toBeDefined();
    expect(function() {
      pureCloudService.presence.getUserPresences();
    }).toThrowError('Missing required parameter: userId');
    expect(function() {
      pureCloudService.presence.getUserPresences('1');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.presence.updateUserPresences', function() {
    expect(pureCloudService.presence.updateUserPresences).toBeDefined();
    expect(function() {
      pureCloudService.presence.updateUserPresences();
    }).toThrowError('Missing required parameter: userId');
    expect(function() {
      pureCloudService.presence.updateUserPresences('1');
    }).toThrowError('Missing required parameter: source');
    expect(function() {
      pureCloudService.presence.updateUserPresences('1', 'PURECLOUD');
    }).toThrowError('Missing required parameter: body');
    expect(function() {
      pureCloudService.presence.updateUserPresences('1', 'PURECLOUD', 'body');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.presence.setMyStatus', function() {
    expect(pureCloudService.presence.setMyStatus).toBeDefined();
    expect(function() {
      pureCloudService.presence.setMyStatus();
    }).toThrowError('Missing required parameter: statusKey');
    expect(function() {
      pureCloudService.presence.setMyStatus('STATUS');
    }).toThrowError('Invalid statusKey');
  });

  it('pureCloudService.presence.setMeOnQueue', function() {
    expect(pureCloudService.presence.setMeOnQueue).toBeDefined();
    expect(function() {
      pureCloudService.presence.setMeOnQueue();
    }).toThrowError('Invalid statusKey');
  });

  it('pureCloudService.presence.setMeOffQueue', function() {
    expect(pureCloudService.presence.setMeOffQueue).toBeDefined();
    expect(function() {
      pureCloudService.presence.setMeOffQueue();
    }).toThrowError('Invalid statusKey');
  });
  /* END: presence */

  /* BEGIN: notifications */
  it('pureCloudService.notifications.getAvailableTopics', function() {
    expect(pureCloudService.notifications.getAvailableTopics).toBeDefined();
    expect(function() {
      pureCloudService.notifications.getAvailableTopics();
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.notifications.getChannels', function() {
    expect(pureCloudService.notifications.getChannels).toBeDefined();
    expect(function() {
      pureCloudService.notifications.getChannels();
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.notifications.createChannel', function() {
    expect(pureCloudService.notifications.createChannel).toBeDefined();
    expect(function() {
      pureCloudService.notifications.createChannel();
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.notifications.getChannelSubscriptions', function() {
    expect(pureCloudService.notifications.getChannelSubscriptions).toBeDefined();
    expect(function() {
      pureCloudService.notifications.getChannelSubscriptions();
    }).toThrowError('Missing required parameter: channelId');
    expect(function() {
      pureCloudService.notifications.getChannelSubscriptions('u7ggkk2jir2mmngsnagkhbko88');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.notifications.addChannelSubscriptions', function() {
    expect(pureCloudService.notifications.addChannelSubscriptions).toBeDefined();
    expect(function() {
      pureCloudService.notifications.addChannelSubscriptions();
    }).toThrowError('Missing required parameter: channelId');
    expect(function() {
      pureCloudService.notifications.addChannelSubscriptions('u7ggkk2jir2mmngsnagkhbko88');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.notifications.replaceChannelSubscriptions', function() {
    expect(pureCloudService.notifications.replaceChannelSubscriptions).toBeDefined();
    expect(function() {
      pureCloudService.notifications.replaceChannelSubscriptions();
    }).toThrowError('Missing required parameter: channelId');
    expect(function() {
      pureCloudService.notifications.replaceChannelSubscriptions('u7ggkk2jir2mmngsnagkhbko88');
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.notifications.deleteChannelSubscriptions', function() {
    expect(pureCloudService.notifications.deleteChannelSubscriptions).toBeDefined();
    expect(function() {
      pureCloudService.notifications.deleteChannelSubscriptions();
    }).toThrowError('Missing required parameter: channelId');
    expect(function() {
      pureCloudService.notifications.deleteChannelSubscriptions('u7ggkk2jir2mmngsnagkhbko88');
    }).toThrowError('Authentication required!');
  });
  /* END: notifications */

  /* BEGIN: routing */
  it('pureCloudService.routing.changeUserWorkgroupsActivation', function() {
    expect(pureCloudService.routing.changeUserWorkgroupsActivation).toBeDefined();
    expect(function() {
      pureCloudService.routing.changeUserWorkgroupsActivation();
    }).toThrowError('Missing required parameter: memberId');
    expect(function() {
      pureCloudService.routing.changeUserWorkgroupsActivation('1');
    }).toThrowError('Missing required parameter: queueId');
    expect(function() {
      pureCloudService.routing.changeUserWorkgroupsActivation('1', '2');
    }).toThrowError('Authentication required!');
  });
  /* END: routing */

  /* BEGIN: users */
  it('pureCloudService.users.getMe', function() {
    expect(pureCloudService.users.getMe).toBeDefined();
    expect(function() {
      pureCloudService.users.getMe();
    }).toThrowError('Authentication required!');
  });

  it('pureCloudService.users.getUserIdQueues', function() {
    expect(pureCloudService.users.getUserIdQueues).toBeDefined();
    expect(function() {
      pureCloudService.users.getUserIdQueues();
    }).toThrowError('Missing required parameter: userId');
    expect(function() {
      pureCloudService.users.getUserIdQueues('1');
    }).toThrowError('Authentication required!');
  });
  /* END: users */
});