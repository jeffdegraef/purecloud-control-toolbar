'use strict';

describe('Provider: logger', function() {

  beforeEach(module('pureCloudToolbarApp'));

  var $log;
  beforeEach(function() {
    inject(function(_$log_) {
      $log = _$log_;
      $log.setLocalStorageEnabled(true);
    });
  });

  it('$log.clearLogs', function() {
    expect(function() {
      $log.clearLogs();
    }).not.toThrow();
  });

  it('$log.info', function() {
    expect($log.info).toBeDefined();
    expect(function() {
      $log.info('it worked!');
    }).not.toThrow();
    var c = $log.exportLogs();
    expect(c).toMatch('it worked!');
  });

  it('$log.clearLogs', function() {
    expect(function() {
      $log.clearLogs();
    }).not.toThrow();
  });
});