# pure-cloud-toolbar

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Install

* Install node from nodejs.org
* Install Ruby (http://rubyinstaller.org/downloads/). Make sure you check the "Add Ruby executables to your PATH" option.
* git clone this repo
* Run the following commands using an administrator cmd (or use sudo from Mac) from the root folder of the source code
    * `npm install -g grunt-cli bower yo generator-karma generator-angular karma` (might require `sudo`  on Mac)
    * `npm install` (might require `sudo` on Mac)
    * `bower install`
    * `gem install compass` (might require `sudo` on Mac)

## Build & development

Run `grunt serve` to start the server.

## Testing

Running `grunt test` will run the unit tests with karma.

## UI Testing

UI tests use [protractor](http://angular.github.io/protractor/#/ "Protractor")

* Update webdriver-manager: `webdriver-manager update`
* Install driver for IE: `webdriver-manager update --ie`
* Start Web Driver: `webdriver-manager start`
* Start the web server: `grunt serve`
* Run the tests: `protractor uitest\\loader\\conf.js` (or `protractor uitest/loader/conf.js` if you are using a Mac) 

Note: when testing popups, it is recommended to only test with one browser at a time. Change the `uitest/loader/conf.js` accordingly. IE tends to fail when doing popup tests with all browsers at the same time.

## Build

Run `grunt` for building