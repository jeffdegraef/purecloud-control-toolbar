exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['loader.js'],
  multiCapabilities: [
    {
      browserName: 'chrome'
    }
    , 
    {
      browserName: 'firefox'
    }
    , 
    {
      browserName: 'internet explorer',
      platform: 'ANY',
      version: '11'
    }
  ]
};