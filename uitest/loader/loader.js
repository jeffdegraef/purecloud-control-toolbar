var baseUrl = 'http://localhost:9000';
var renderPage = 'testrender.html';

describe('Basic UI Tests', function() {
  beforeEach(function() {
    // To test non-angular pages
    return browser.ignoreSynchronization = true;
  });

  it('should have a title', function() {
    browser.get(baseUrl + '/' + renderPage);
    expect(browser.getTitle()).not.toBeNull();
  });

  it('should not contain google (i.e. homepage)', function() {
    browser.get(baseUrl + '/' + renderPage);
    expect(browser.getTitle()).not.toContain('Google');
  });
  
});

describe('Toolbar embed tests', function() {
  beforeEach(function() {
    // To test non-angular pages
    return browser.ignoreSynchronization = true;
  });
  
  it('div should contain an iframe when clicking on the embed button', function() {
    browser.get(baseUrl + '/' + renderPage);

    element(by.id('btnEmbed')).click();
    
    var pureCloudToolbarContainer = element(by.id('pureCloudToolbarContainer')); 
    expect(pureCloudToolbarContainer.getInnerHtml()).toContain('iframe');
  });
  
});

describe('Toolbar popup tests', function() {
  beforeEach(function() {
    // To test non-angular pages
    return browser.ignoreSynchronization = true;
  });
  
  it('should popup when clicking on the popup button', function() {
    browser.get(baseUrl + '/' + renderPage);

    element(by.id('btnPopup')).click();
    expect(selectWindow(1)).toBeNull(); // Select popup window
    expect(element(by.id('PanelSignedOff')).getInnerHtml().length).not.toBeNull();
  });

});

// Functions
global.selectWindow = function(index) {
  // wait for handles[index] to exist
  browser.wait(function() {
    return browser.getAllWindowHandles().then(function(handles) {
      /**
       * Assume that handles.length >= 1 and index >=0.
       * So when calling selectWindow(index) return
       * true if handles contains that window.
       */
      if (handles.length > index) {
        return true;
      }
    });
  }, 30000);
  // here i know that the requested window exists. Switch to the window.
  return browser.getAllWindowHandles().then(function(handles) {
    return browser.switchTo().window(handles[index]);
  });
};